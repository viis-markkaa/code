public double median(int[] arr)
{
    Arrays.sort(arr);
    return arr.length % 2 == 0 
        ? (arr[(int) Math.floor(arr.length/2) - 1] + arr[(int)Math.floor(arr.length/2)])/2.0 
        : arr[(int) arr.length/2];   
}