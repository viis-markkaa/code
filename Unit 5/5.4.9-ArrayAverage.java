public class ArrayAverage
{
   private int[] values;

   public ArrayAverage(int[] theValues)
   {
      values = theValues;
   }

   public double getAverage()
   {
     int t = 0; 
     for (int i:values)t+=i;
     return (0.0d+t)/values.length;
   }
}