import java.util.ArrayList;

public class Odd extends ConsoleProgram
{
    public void run()
    {
        ArrayList<Integer> odds = new ArrayList<Integer>();
        for(int index = 1; index <101; index++) odds.add(index); 
        this.removeEvens(odds);
    }
    
    public static void removeEvens(ArrayList<Integer> array)
    {
        for (int i = 0; i < array.size(); i++) if (array.get(i) % 2 == 0) array.remove(i);
        for (int i = 0; i < array.size(); i++) System.out.println(array.get(i)); // society
    }
}