/*
 * YOU DO NOT NEED TO CHANGE ANYTHING FOR THIS ASSIGNMENT; YOU CAN JUST SUBMIT 
 */

public class TwoDArray {

    private Object[][] myArray;

    public TwoDArray(Object[][] input){
        myArray = input;
    }

    public boolean equals(Object[][] other) {
        if (this.myArray.length != other.length) return false;
        for (int i = 0; i < this.myArray.length; i++) {
            if (this.myArray[i].length != other[i].length) return false;
            for (int j = 0; j < this.myArray[i].length; j++) {
                if (!this.myArray[i][j].equals(other[i][j])) return false;
            }
        }
        return true;
    }
}