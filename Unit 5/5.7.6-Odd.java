/*
 * YOU DO NOT NEED TO CHANGE ANYTHING FOR THIS ASSIGNMENT; YOU CAN JUST SUBMIT 
 */

public class Odd extends ConsoleProgram
{
    public void run()
{
        // Run this code first to see what is does. 
        // Then replace the for loop with an equivalent while loop.
        for (int x = 1; x <= 10; x = x+2)
            System.out.println(x);
    }
}