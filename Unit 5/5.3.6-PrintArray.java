public class PrintArray extends ConsoleProgram
{
    public void run()
    {
        String[] arr = new String[]{"a", "b", "c"};
        printArr(arr);
    }
    
    public void printArr(String[] arr)
    {
        for (String s : arr) println(s);
    }
}