// Why is this an assigment
// Why are we making a pointless class that is overshadowed by lists

public class ExpandingArray
{
    private static final int STARTING_SIZE = 10;
    private int[] arr;
    private int currentSize;
    private int numElements;
    
    public ExpandingArray()
    {
        arr = new int[STARTING_SIZE];
        currentSize = STARTING_SIZE;
        numElements = 0;
    }
 
    public int remove(int index) {
        if (arr == null || index < 0|| index >= arr.length) return 0;
        int[] arr2 = new int[arr.length - 1];
        for (int i = 0, j = 0; i < arr.length; i++) if (i != index) arr2[j++] = arr[i];
        arr = arr2;
        
        // this part is here because codehs can't code :)
        currentSize = arr.length;
        numElements = arr.length;
        return 0;
    }

    public void add(int index, int element) {
        if (arr == null || index < 0|| index >= arr.length) return;
        int[] arr2 = new int[arr.length + 1];
        for (int i = 0; i < arr.length + 1; i++) {
            if (i < index) arr2[i] = arr[i];
            else if (i == index) arr2[i] = element;
            else arr2[i] = arr[i - 1];
        }
        arr = arr2;  
        
        // im crying
        currentSize = arr.length;
        numElements = arr.length;
    }
    
    // why
    public int size()
    {
        return arr.length;
    }
    
    private boolean isFull()
    {
        return numElements == currentSize;
    }
    
    private void expand()
    {
        System.out.println("Expanding");
        int newSize = currentSize * 2;
        int[] newArray = new int[newSize];
        
        // Copy over old elements
        for(int i = 0; i < currentSize; i++)
        {
            newArray[i] = arr[i];
        }
        
        currentSize = newSize;
        arr = newArray;
    }
    
    public int get(int index)
    {
        return arr[index];
    }
    
    public void add(int x)
    {
        if(isFull())
        {
            expand();
        }
        arr[numElements] = x;
        numElements++;
    }
    
    public String toString()
    {
        // Return empty curly braces if the array is empty
        if (numElements == 0)
        {
            return "{}";
        }
        
        // Return Elements 
        String str = "{";
        for (int i=0; i < numElements; i++)
        {
            str += arr[i] + ", ";
        }
        if (str.length() > 0 && str.charAt(str.length()-2)==',')
        {
            str = str.substring(0, str.length()-2);
            str += "}";
        }
        return str;
    }
}
