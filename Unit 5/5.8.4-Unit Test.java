public List<Book> filterBooks(List<Book> readingList, int maxPages)
{
    List<Book> res = new ArrayList<Book>();
    for (int i = 0; i < readingList.size(); i++) {
        if (readingList.get(i).getNumPages() <= maxPages) res.add(readingList.get(i));
    }
    return res;
    // this would work but i cant import java.util.* here
    // return readingList.stream().filter(b -> b.getNumPages() <= maxPages).collect(Collectors.toList());
}
