import java.util.*;
import java.util.stream.Collectors;

public class BinaryConversion extends ConsoleProgram
{
    public String binaryToText(String binary) {
        // absolutely devious
        return Arrays.asList(binary.split("(?<=\\G.{8})")).stream().map(b -> Integer.parseInt(b, 2)).map(d -> Character.toString((char) ((int) d))).collect(Collectors.joining(""));
    }
}
