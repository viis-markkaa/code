import java.util.*;

public class Classroom
{
    Student[] students;
    int numStudentsAdded;
    
    public Classroom(int numStudents)
    {
        students = new Student[numStudents];
        numStudentsAdded = 0;
    }
    
    public Student getMostImprovedStudent()
    {
        Student mis = null; 
        for (int i = 0; i < numStudentsAdded; i++) {
            if (Objects.isNull(mis) || mis.getExamRange() < students[i].getExamRange()) mis = students[i];
        }
        return mis;
    }
    
    public void addStudent(Student s)
    {
        students[numStudentsAdded] = s;
        numStudentsAdded++;
    }
    
    public void printStudents()
    {
        for(int i = 0; i < numStudentsAdded; i++)
        {
            System.out.println(students[i]);
        }
    }
}