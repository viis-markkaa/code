import java.util.*;

public class WordCounts extends ConsoleProgram
{
    public void run()
    {
        // It's funny, when I used this one, it kept looping infinitely without an error
        // Scanner in = new Scanner(System.in);
        // String[] sp = in.nextLine().toLowerCase().split("\\s+");
        String[] sp = readLine("Enter a string: ").toLowerCase().split("\\s+");
        HashMap<String, Integer> words = new HashMap<>();
        for (String str : sp) {
            if (Objects.isNull(words.get(str))) words.put(str, 1);
            else words.put(str, words.get(str) + 1);
        }
        printSortedHashMap(words);
    }

    private void printSortedHashMap(HashMap<String, Integer> wordCount){
        Object[] keys = wordCount.keySet().toArray();
        Arrays.sort(keys);
        
        for (Object word : keys) {
            int val = wordCount.get(word);
            System.out.println(word + ": " + val);
        }
    }
} 