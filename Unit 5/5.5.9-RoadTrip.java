import java.util.*;

public class RoadTrip
{
    ArrayList<GeoLocation> s = new ArrayList<>();
    
    public RoadTrip() {
        
    }
    
    // Create a GeoLocation and add it to the road trip
    public void addStop(String name, double latitude, double longitude) {
        s.add(new GeoLocation(name, latitude, longitude));
    }

    // Get the total number of stops in the trip
    public int getNumberOfStops() {
        return s.size();
    }

    // Get the total miles of the trip
    public double getTripLength() {
        double t = 0; 
        GeoLocation last = null;
        for (int i = 0; i < s.size(); i++) {
            if (Objects.nonNull(last)) t += s.get(i).distanceFrom(last);
            last = s.get(i);
        }
        return t;
    }

    // Return a formatted toString of the trip
    public String toString() {
        String fin = "";
        for (int i = 0; i < s.size(); i++) {
            GeoLocation a = s.get(i);
            fin += (i + 1) + ". " + a.getName() + " (" + a.getLatitude() + ", " + a.getLongitude() + ")\n";
        }
        return fin;
    }
}