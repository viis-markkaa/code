public int getLastElement(int[] arr)
{
    int len = 0;
    for (int i : arr) len++;
    return arr[len -1];
}