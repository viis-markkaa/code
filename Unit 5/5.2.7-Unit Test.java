public int arrayLength(int[] array)
{
    int tmp = 0;
    for(int i : array) tmp++;
    return tmp;
}