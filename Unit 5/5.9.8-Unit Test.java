public int sumRow(int[][] matrix, int row)
{
    int t = 0;
    for (int i = 0; i < matrix[row].length; i++) t += matrix[row][i];
    return t;
}
