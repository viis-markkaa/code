public ArrayList<String> getAllChoices(String[] flavors, String[] toppings)
{
    ArrayList<String> a = new ArrayList<String>();
    for (int i = 0; i < flavors.length; i++) for (int j = 0; j < toppings.length; j++) a.add(flavors[i] + " with " + toppings[j] + " on top");
    return a;
}
