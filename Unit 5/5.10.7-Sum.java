public class Sum extends ConsoleProgram
{
    public void run()
    {
        int[][] array = {{32, 4, 14, 65, 23, 6},
                        {4, 2, 53, 31, 765, 34},
                        {64235, 23, 522, 124, 42}};
        for(int i = 0; i < 3; i++) System.out.println(sumRow(array, i));
    }
    
    public static int sumRow(int[][] array, int row)
    {
        int t = 0;
        for (int i = 0; i < array[row].length; i++) t += array[row][i];
        return t;
    }
}
