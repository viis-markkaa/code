public int findMinimum(int[] numbers, int length){
    return length == 1 ? numbers[0] : Math.min(numbers[length - 1], findMinimum(numbers, length - 1));
}

