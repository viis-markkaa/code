public int numBacteriaAlive(int hour) {
    return hour == 0 ? 10 : numBacteriaAlive(hour - 1) * 3;
}
