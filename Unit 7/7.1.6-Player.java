import java.util.*;

public class Player {
    private static final int[] SHIP_LENGTHS = {2, 3, 3, 4, 5};
    private Ship[] ships;
    private Grid pg = new Grid();
    private Grid og = new Grid();
    
    private int c = 0;
    
    public Player() {
        ships = new Ship[SHIP_LENGTHS.length];
        for (int i = 0; i < SHIP_LENGTHS.length; i++) ships[i] = new Ship(SHIP_LENGTHS[i]);
    }
    
    public void chooseShipLocation(Ship s, int row, int col, int direction) {
        if (c >= SHIP_LENGTHS.length) return;
        s.setLocation(row, col);
        s.setDirection(direction);
        pg.addShip(s);
        c++;
    }
    
    public void printMyShips() {
        pg.printShips();
    }
    
    public void printOpponentGuesses() {
        og.printStatus();
    }
    
    public void printMyGuesses() {
        pg.printStatus();
    }

    public boolean recordOpponentGuess(int row, int col) {
        if (pg.hasShip(row, col)) og.markHit(row, col);
        else og.markMiss(row, col);
        return pg.hasShip(row, col);
    }
}