import java.util.*;

public class Grid {
  private Location[][] grid;

  // Constants for number of rows and columns.
  public static final int NUM_ROWS = 10;
  public static final int NUM_COLS = 10;

  // Create a new Grid. Initialize each Location in the grid
  // to be a new Location object.
  public Grid() {
      grid = new Location[NUM_ROWS][NUM_COLS];
      for (int i = 0; i < NUM_ROWS; i++) {
          for(int j = 0; j < NUM_COLS; j++) grid[i][j] = new Location();
      }
          
  }

  // Mark a hit in this location by calling the markHit method
  // on the Location object.  
  public void markHit(int row, int col) {
      grid[row][col].setStatus(1);
  }

  // Mark a miss on this location.    
  public void markMiss(int row, int col) {
      grid[row][col].setStatus(2);
  }

  // Set the status of this location object.
  public void setStatus(int row, int col, int status) {
      grid[row][col].setStatus(status);
  }

  // Get the status of this location in the grid  
  public int getStatus(int row, int col) {
      return grid[row][col].getStatus();
  }

  // Return whether or not this Location has already been guessed.
  public boolean alreadyGuessed(int row, int col) {
      return grid[row][col].getStatus() > 0;
  }

  // Set whether or not there is a ship at this location to the val   
  public void setShip(int row, int col, boolean val) {
      grid[row][col].setShip(val);
  }

  // Return whether or not there is a ship here   
  public boolean hasShip(int row, int col) {
      return grid[row][col].hasShip();
  }

  // Get the Location object at this row and column position
  public Location get(int row, int col) {
      return grid[row][col];
  }

  // Return the number of rows in the Grid
  public int numRows() {
      return grid.length;
  }

  // Return the number of columns in the grid
  public int numCols() {
      return grid[0].length;
  }

  public void printStatus() {
      System.out.print("  1 2 3 4 5 6 7 8 9 10\n");
      char[] letters = new char[]{'A','B','C','D','E','F','G','H','I','J'};
      for (int row = 0; row < letters.length; row++) {
          System.out.print(letters[row] + " ");
          for(int i = 0; i < grid[row].length; i++) System.out.print(grid[row][i].getStatus() == 0 ? "- " : (grid[row][i].getStatus() == 1 ? "X " : "O "));
          System.out.print("\n");
      }
  }

  public void printShips() {
      System.out.print("  1 2 3 4 5 6 7 8 9 10\n");
      char[] letters = new char[]{'A','B','C','D','E','F','G','H','I','J'};
      for (int row = 0; row < letters.length; row++) {
          System.out.print(letters[row] + " ");
          for(int i = 0; i < grid[row].length; i++) System.out.print(!grid[row][i].hasShip() ? "- " : "X ");
          System.out.print("\n");
      }
  }
}