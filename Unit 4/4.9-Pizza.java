public class Pizza
{
    String type;
    String toppings;
    int size;
    public Pizza(String theType, String theToppings, int theSize) {
        type = theType;
        toppings = theToppings;
        size = theSize;
    }
    public String toString() {
        return type + " " + toppings + " " + size;
    }
}