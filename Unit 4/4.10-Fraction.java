public class Fraction
{
       int n = 1;
       int d = 2;
       
       public Fraction (int num, int den) {
           n = num;
           d = den;
       }
       
       public String toString() {
           return n + "/" + d;
       }
}