public class Dragon 
{
    private String name;
    private int level;
    private boolean canBreatheFire;
    
    // Write the constructor here!
    
    public Dragon(String n, int l) {
        this.name = n;
        this.level = l;
        this.canBreatheFire = l >= 70;
    }
    
    // Put getters here
    public String getName() {
        return this.name;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    public boolean isFireBreather() {
        return this.canBreatheFire;
    }
    
    // Put other methods here
    
    public void gainExperience() {
        this.level += 10;
        this.canBreatheFire = this.level >= 70;
    }
    
    public void attack() {
        if (this.canBreatheFire) {
            String tmp = "";
            for (int i = 0; i < 10; i++) tmp += ">";
            tmp += "\n";
            for (int i = 0; i < 14; i++) tmp += ">";
            tmp += "\n";
            for (int i = 0; i < 14; i++) tmp += ">";
            tmp += "\n";
            for (int i = 0; i < 10; i++) tmp += ">";
            System.out.println(tmp);
        } else {
            System.out.println("~ ~ ~");
        }
    } 

    // String representation of the object
    public String toString()
    {
        return "Dragon " + name + " is at level " + level;
    }
}