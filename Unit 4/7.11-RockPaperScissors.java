public class RockPaperScissors extends ConsoleProgram
{
    private boolean playing = true;
    private String prompt = "Enter your choice (rock, paper, or scissors):";
    
    private String getWinner(String user, String computer) {
        String USER_PLAYER = "User wins!";
        String COMPUTER_PLAYER = "Computer wins!";
        String TIE = "Tie";
        
        if (user.equals("")) {
            playing = false;
            return "Thanks for playing!";
        }

        System.out.println("User: " + user);
        System.out.println("Computer: " + computer);
    
        if (user.equals("rock")) {
            if (computer.equals("paper")) return COMPUTER_PLAYER;
            else if (computer.equals("scissors")) return USER_PLAYER;
        } else if (user.equals("paper")) {
            if (computer.equals("scissors")) return COMPUTER_PLAYER;
            else if (computer.equals("rock")) return USER_PLAYER;
        } else if (user.equals("scissors")) {
            if (computer.equals("rock")) return COMPUTER_PLAYER;
            else if (computer.equals("paper")) return USER_PLAYER;
        }
        return TIE;
    }
    
    public void run() {
        while (playing) {
            String player = readLine(prompt);
            double rand = Math.random();
            if (rand < .33) System.out.println(getWinner(player, "rock"));
            else if (rand < .66) System.out.println(getWinner(player, "paper"));
            else System.out.println(getWinner(player, "scissors"));
        }
    }
}