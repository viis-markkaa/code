public class Rectangle{

    private int width;
    private int height;
     
    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }
    
   // Add toString and equals methods
   public String toString() {
       return "A rectangle with a width of " + this.width + " and a height of " + this.height;
   }
   
   public boolean equals(Rectangle o) {
        return (this.width == o.width && this.height == o.height) || (this.width == o.height && this.height == o.width);
   }
}