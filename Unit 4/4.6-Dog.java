public class Dog 
{
    private String breed;
    // Add an instance variable here for name.
    private String name;
    
    public Dog(String theBreed, String tname)
    {
        breed = theBreed;
        name = tname;
    }
    
    public String toString()
    {
        return name + " is a " + breed;
    }
}