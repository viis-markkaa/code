public class Dragon 
{
    private String attack;
    private int level;
    
    // Write the constructor here!
    public Dragon(int l, String attack) {
        this.level = l;
        this.attack = attack;
    }
    
    
    // Put getters here
    
    public String getAttack() {
        return this.attack;
    }
    
    public int getLevel() {
        return this.level;
    }
    
    // Put other methods here
    
    public String fight() {
        String s = "";
        for (int i = 0; i < this.level; i++) s += attack;
        return s;
    }

    // String representation of the object
    public String toString()
    {
        return "Dragon is at level " + level + " and attacks with " + attack;
    }
}