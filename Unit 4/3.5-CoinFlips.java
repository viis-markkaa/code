public class CoinFlips extends ConsoleProgram
{
    public static final int FLIPS = 100;
    
    public void run()
    {
        int heads = 0;
        int tails = 0;
        for(int i = 0; i < 100; i++) {
            String coin = Randomizer.nextBoolean() ? "Heads" : "Tails";
            System.out.println(coin);
            if (coin == "Heads") heads++;
            else tails++;
        }
        System.out.println("Heads: " + heads);
        System.out.println("Tails: " + tails);
        System.out.println("% Heads: " + heads/100f);
        System.out.println("% Tails: " + tails/100f);
    }
}