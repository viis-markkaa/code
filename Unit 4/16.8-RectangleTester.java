public class RectangleTester extends ConsoleProgram
{
    public void run()
    {
        Rectangle rect1 = new Rectangle(5, 4);
        Rectangle rect2 = new Rectangle(5, 4);
        Rectangle rect3 = new Rectangle(10, 4);
        
        // Print all three rectangles
        println(rect1);
        println(rect2);
        println(rect3);
        
        // Print one true statement comparing rectangles
        println(true);
        
        // Print one false statment comparing rectangles
        println(false);
        
    }
}