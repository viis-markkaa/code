public class Fraction
{
    private int n;
    private int d;
    
    public Fraction(int n, int d) {
        this.n = n;
        this.d = d;
    }
    
    public int getNumerator() {
    return n;
    }
    
    public int getDenominator() {
        // IMPLEMENT THIS METHOD
        return d;
    }
    
    public void setNumerator(int x) {
        // IMPLEMENT THIS METHOD
        n = x;
    }
    
    
    public void setDenominator(int x) {
        // IMPLEMENT THIS METHOD
        d = x;
    }
    
    public void add(Fraction other) {
        // IMPLEMENT THIS METHOD
        this.n = this.n * other.d + other.n * this.d;
        this.d = this.d * other.d;
    }
    
    public void subtract(Fraction other) {
        // IMPLEMENT THIS METHOD
        this.n = this.n * other.d - other.n * this.d;
        this.d = this.d * other.d;
    }
    
    public void multiply(Fraction other) {
        // IMPLEMENT THIS METHOD
        this.n *= other.n;
        this.d *= other.d;
    }
    
    public String toString() {
        // IMPLEMENT THIS METHOD
        return (this.n + " / " + this.d);
    }
}