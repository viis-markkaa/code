public class Fraction
{
       private int num;
       private int den;
       
       public Fraction(int n, int d) {
           num = n;
           den = d;
       }
       
       public int getNumerator() {
           return num;
       }
public int getDenominator() {
    return den;
}
public void setNumerator(int x) {
    num = x;
}
public void setDenominator(int x){
    den = x;
}
}