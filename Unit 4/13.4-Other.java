public class Jeans extends Clothing
{
    public Jeans(String size) {
        super(size, "blue");
    }
}

public class Sweatshirt extends Clothing
{
    private boolean hooded;
    public Sweatshirt(String size, String color, boolean hooded) {
        super(size, color);
        this.hooded = hooded;
    }
    
    public boolean hasHood() {
        return hooded;
    }
}

public class TShirt extends Clothing
{
    private String fabric;
    public TShirt(String size, String color, String fabric) {
        super(size, color);
        this.fabric = fabric;
    }
    
    public String getFabric() {
        return fabric;
    }
}