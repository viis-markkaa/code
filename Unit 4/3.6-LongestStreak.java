public class LongestStreak extends ConsoleProgram
{
    public static final int FLIPS = 100;
    
    public void run()
    {
        int streak = 0;
        int longest = 0;
        for (int i = 0; i < FLIPS; i++) {
            String coin = Randomizer.nextBoolean() ? "Heads" : "Tails";
            System.out.println(coin);
            if (coin == "Heads") streak++;
            else streak = 0;
            if (streak > longest) longest = streak;
        }
        System.out.println("Longest streak of heads: " + longest);
    }
}