public class UnitCircle extends ConsoleProgram
{
    public void run()
    {
        println("Radians: (cos, sin)");
        for(int i = 0; i <= 7; i++) System.out.println((i * Math.PI/4) + ": " + Math.cos((i * Math.PI/4)) + ", " + Math.sin((i * Math.PI/4)));
    }
}