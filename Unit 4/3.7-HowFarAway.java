public class HowFarAway extends ConsoleProgram
{
    public void run()
    {
        double a = readDouble(":");
        double b = readDouble(":");
        double c = readDouble(":");
        double d = readDouble(":");
        GeoLocation ab = new GeoLocation(a, b);
        GeoLocation cd = new GeoLocation(c, d);
        System.out.println("The distance is " + ab.distanceFrom(cd) + " miles.");
    }
}