public class ExtremeMain extends ConsoleProgram
{
    public void run()
    {
        
        // Create an Extremes object
        Extremes ex = new Extremes();
        
        // Ask the user to guess the maximum value of an Integer
        int g = readInt("");
        
        // Compute and display the difference
        // between the max and the guess
        println("You were off by " + ex.maxDiff(g));
        
        // Ask the user to guess the minimum value of an Integer
        g = readInt("");
        
        // Compute and display the difference 
        // between the min and the guess
        println("You were off by " + Math.abs(ex.minDiff(g)));
        
    }
}