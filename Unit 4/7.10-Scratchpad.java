private String getWinner(String user, String computer)
{
    // Return the proper string for the result.
    String USER_PLAYER = "User wins!";
    String COMPUTER_PLAYER = "Computer wins!";
    String TIE = "Tie";

    if (user.equals("rock")) {
        if (computer.equals("paper")) return COMPUTER_PLAYER;
        else if (computer.equals("scissors")) return USER_PLAYER;
    } else if (user.equals("paper")) {
        if (computer.equals("scissors")) return COMPUTER_PLAYER;
        else if (computer.equals("rock")) return USER_PLAYER;
    } else if (user.equals("scissors")) {
        if (computer.equals("rock")) return COMPUTER_PLAYER;
        else if (computer.equals("paper")) return USER_PLAYER;
    }
    return TIE;
}